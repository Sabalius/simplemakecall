#pragma once
#include <gclib.h>
#include <gcip.h>
#include <memory>
#include "ProgramConfig.h"
#include "ProgramLog.h"
#include "locked_queue.h"

IPCCLIB_START_DATA * InitIPVirtboards(const ProgramConfig& _config);
void StartGlobalCall(const ProgramConfig& _config);
long OpenBoard();
LINEDEV OpenLine(int nTimeslot);
void StopGlobalCall();
long EventHandlerStub(unsigned long ulParam);

class TestRunner
{
public:
	TestRunner(int argc, char ** argv) : config(argc, argv), iterations(config.getIterations())
	{
	}

	static TestRunner& getHolder(int argc = 0, char ** argv = nullptr)
	{
		if(!holder)
			holder = std::make_unique<TestRunner>(argc, argv);

		return *holder;
	}


	ProgramLog&		getLog() { return log; }
	ProgramConfig&	getConfig() { return config; }

	void			increaseCurrentCalls()			{ currentCalls.fetch_add(1); }
	int				decreaseCurrentCalls()			{ return currentCalls.fetch_sub(1); }
	
	void			markLineAsFree(LINEDEV _ldev)	
	{ 
		freeLines.push(_ldev); 
	}
	
	void			setCallNumber(LINEDEV _ldev, const string& _number)	{
		allLines[_ldev] = _number;
	}

	string			getNumber(LINEDEV _line) { 
		try {
			return allLines.at(_line);
		}
		catch (std::out_of_range&) {
			return "";
		}
	}

	void setupGlobalCall() {
		StartGlobalCall(config);
		EnableHandler();
		lBoard = OpenBoard();
	}

	int				iterate() { 
		return iterations.fetch_sub(1); 
	}

	void			beginTest() {
		for (auto i : allLines)
			markLineAsFree(i.first);
	}

	void			openAllLines() {
		for (int i = 0; i < config.getLinesCount(); ++i) {
			LINEDEV line = OpenLine(i + 1);

			wostringstream out;
			out << L"LINE DEVICE OPENED " << line << endl;
			log.Log(out.str());
			allLines[line] = "No call";
		}

	}

	void			closeAllLines() {
		for (const auto& i : allLines) {
			gc_Close(i.first);
		}
	}

	void tearDownGlobalCall() {
		DisableHandler();
		gc_Close(lBoard);

		StopGlobalCall();
	}

	long getBoard() const { return lBoard; }

	bool			nextMakeCall();

private:
	bool EnableHandler() {
		return sr_enbhdlr(EV_ANYDEV, EV_ANYEVT, EventHandlerStub) != -1;
	}

	bool DisableHandler() {
		return sr_dishdlr(EV_ANYDEV, EV_ANYEVT, EventHandlerStub) != -1;
	}

	ProgramLog		log;
	ProgramConfig	config;
	atomic<int> currentCalls = 0;
	atomic<int> iterations = 0;
	locked_queue<LINEDEV> freeLines;
	map<LINEDEV, string> allLines;
	long lBoard;

	static std::unique_ptr<TestRunner> holder;
};

class CGCParamBlock
{
	GC_PARM_BLKP	m_gcParamBlk;
	bool			m_blAutoDelete;
public:
	CGCParamBlock() : m_gcParamBlk(NULL), m_blAutoDelete(true) {

	}

	CGCParamBlock(GC_PARM_BLKP pParamBlock) : m_gcParamBlk(pParamBlock), m_blAutoDelete(false) {
	}

	~CGCParamBlock() {
		if (m_blAutoDelete && !is_null())
		{
			gc_util_delete_parm_blk(m_gcParamBlk);
		}
	}

	GC_PARM_BLKP * get_pointer() { return &m_gcParamBlk; }
	GC_PARM_BLKP  get_block() { return m_gcParamBlk; }
	bool is_null() const { return m_gcParamBlk == NULL; }
};


void SetCapability(CGCParamBlock& target_datap, IP_CAPABILITY& t_Capability, int direction);
CRN MakeSipIpCall(TestRunner& holder,  LINEDEV ldev);

