// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>



// TODO: reference additional headers your program requires here
#include <iostream>
#include <sstream>
#include <string>
#include <map>
#include <atomic>
#include <future>
#include <thread>
#include <chrono>
#include <iomanip>
#include <ctime>
#include <fstream>
#include <cvt/wstring>
#include <codecvt>
#include <locale.h>
