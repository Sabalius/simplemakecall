#ifndef LOCKEDQUEUE_H
#define LOCKEDQUEUE_H

#include <queue>
#include <thread>
#include <condition_variable>
#include <mutex>
using namespace std;

template <typename T>
class locked_queue
{
	typedef std::mutex					mutex;

	std::queue<T>						m_queue;
	std::condition_variable				m_cond;
	mutex								m_mut;

public:
	locked_queue() 
	{

	}

	~locked_queue()
	{

	}

	void push(const T& _value) {
		std::lock_guard<mutex> lock(m_mut);
		m_queue.push(_value);
		m_cond.notify_one();
	}

	void push(T&& _value) {
		std::lock_guard<mutex> lock(m_mut);
		m_queue.push(std::move(_value));
		m_cond.notify_one();
	}

	
	T& front() {
		std::unique_lock<mutex> lock(m_mut);
		m_cond.wait(lock, std::bind(&locked_queue<T>::not_empty, this));
		return m_queue.front();
	}
	
	void pop() {
		std::unique_lock<mutex> lock(m_mut);
		m_cond.wait(lock, std::bind(&locked_queue<T>::not_empty, this));
		m_queue.pop();
	}

	void pop_front(T& _value) {
		std::unique_lock<mutex> lock(m_mut);
		m_cond.wait(lock, std::bind(&locked_queue<T>::not_empty, this));
		_value = m_queue.front();
		m_queue.pop();
	}

	T pop_front() {
		std::unique_lock<mutex> lock(m_mut);
		m_cond.wait(lock, std::bind(&locked_queue<T>::not_empty, this));
		T result = m_queue.front();
		m_queue.pop();
		return result;
	}

	bool empty() {
		return m_queue.empty();
	}

	bool not_empty() {
		return !m_queue.empty();
	}

};

#endif //LOCKEDQUEUE_H
