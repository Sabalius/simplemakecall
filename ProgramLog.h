#pragma once
#include <mutex>
#include <string>
#include <fstream>
#include <iomanip>
#include <ctime>
#include <fstream>
#include <cvt/wstring>
#include <codecvt>
#include <locale.h>
#include <gclib.h>
#include <gcip.h>

using namespace std::chrono;
using clock_type = std::chrono::system_clock;
using time_type = std::chrono::time_point<clock_type>;


inline std::string wtos(const std::wstring& in)
{
	typedef std::codecvt_utf8<wchar_t> convert_typeX;
	std::wstring_convert<convert_typeX, wchar_t> converterX;
	return converterX.to_bytes(in);
}

inline std::wstring stow(const std::string& in, std::locale loc = std::locale())
{
	typedef std::codecvt_utf8<wchar_t> convert_typeX;
	std::wstring_convert<convert_typeX, wchar_t> converterX;
	return converterX.from_bytes(in);
}

inline uint64_t get_milliseconds(const time_type& _time_point)
{
	using namespace std::chrono;
	milliseconds ms = duration_cast<milliseconds>(_time_point.time_since_epoch());
	return ms.count() % 1000;
}

template<typename Stream>
Stream& operator << (Stream& _stream, const time_type& _time_point)
{
	using namespace std::chrono;

	std::time_t t = clock_type::to_time_t(_time_point);
	auto fractional_seconds = get_milliseconds(_time_point);

	struct tm time;
	gmtime_s(&time, &t);

	_stream << std::put_time(&time, L"%Y-%m-%d %H:%M:%S") << L'.' << std::dec << std::setw(3) << std::setfill(L'0') << fractional_seconds;
	return _stream;
}

class ProgramLog
{
	std::mutex log_access;
	std::wofstream log_file;

public:
	ProgramLog() : log_file("./MakeCall.log", std::ios_base::out | std::ios_base::app)
	{
		log_file << L"===START===============================================================================================================================" << std::endl;
	}

	~ProgramLog()
	{
		log_file << L"===FINISH==============================================================================================================================" << std::endl;

		log_file.flush();
		log_file.close();

	}

	void LogCallProcessing(const std::wstring& _state, const std::wstring& _number, LINEDEV _line, CRN _crn)
	{
		std::lock_guard<std::mutex> lock(log_access);
		log_file << clock_type::now() << L"\tCALL: " << _number << L" " << _state << L"\tLINEDEV: " << _line << L"\tCRN: " << std::hex << std::uppercase << std::setfill(L'0') << _crn << std::endl;
	}

	void Log(const std::wstring& _message)
	{
		std::lock_guard<std::mutex> lock(log_access);
		log_file << clock_type::now() << L"\t" << _message << std::endl;
	}
};