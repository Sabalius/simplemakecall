// DialogicLeakTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "locked_queue.h"
#include "ProgramConfig.h"
#include "ProgramLog.h"
#include "Dialogic.h"

//#pragma warning (disable : 4267)

using namespace std;

void CallMakingThread(TestRunner& holder) {
	while (holder.nextMakeCall()) {
	}
}

void printAndWaitInput(const wstring& _output) {
	wcout << _output << endl;
	wchar_t strData[256];
	wcin.getline(strData, sizeof(strData));
}

int main(int argc, char * argv[]) {
	if (argc != 6) {
		wcout << L"Invalid number of arguments" << endl;
		wcout << L"Usage: SimpleMakeCall.exe <number of lines> <number of iterations> <timeout(ms) between GCEV_RELEASECALL and gc_MakeCall> <ip from> <number to call>" << endl;
		wcout << L"Example: SimpleMakeCall.exe 2 100 50 \"172.16.72.154\" \"100@172.16.72.152\"" << endl;
		return 0;
	}

	printAndWaitInput(L"Press [Enter] to start the test.");

	TestRunner& holder = TestRunner::getHolder(argc, argv);
	auto& config = holder.getConfig();
	auto& log = holder.getLog();

	wcout << L"MakeCall.log file created" << endl;

	holder.setupGlobalCall();
	holder.openAllLines();

	printAndWaitInput(L"All lines are open. Press [Enter] to make calls.");

	thread calling(&CallMakingThread, ref(holder));
	holder.beginTest();
	calling.join();

	printAndWaitInput(L"Press [Enter] to close IP time slot and board and stop the GlobalCall");

	holder.closeAllLines();
	holder.tearDownGlobalCall();

	printAndWaitInput(L"Press [Enter] to stop the test");

	return 0;
}

