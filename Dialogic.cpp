#include "stdafx.h"
#include "Dialogic.h"
#include <gcip.h>

std::unique_ptr<TestRunner> TestRunner::holder;

IPCCLIB_START_DATA * InitIPVirtboards(const ProgramConfig& _config) {
	static IP_VIRTBOARD			ip_virtboard[1];
	static IPCCLIB_START_DATA	ipcclibstart;
	static SIP_STACK_CFG stack = { 0 };
	
	memset(&ipcclibstart, 0, sizeof(IPCCLIB_START_DATA));
	memset(ip_virtboard, 0, sizeof(IP_VIRTBOARD));

	INIT_IPCCLIB_START_DATA(&ipcclibstart, 1, &ip_virtboard[0]);
	INIT_IP_VIRTBOARD(&ip_virtboard[0]);
	INIT_SIP_STACK_CFG(&stack);

	ipcclibstart.delimiter = ',';
	ipcclibstart.num_boards = 1;
	ipcclibstart.board_list = ip_virtboard;
	ipcclibstart.max_parm_data_size = 1024;

	ip_virtboard[0].localIP.ip_ver = IPVER4;					// must be set to IPVER4
	ip_virtboard[0].localIP.u_ipaddr.ipv4 = IP_CFG_DEFAULT;			// or specify host NIC IP address
	ip_virtboard[0].h323_signaling_port = 1720;		// or application defined port for H.323
	ip_virtboard[0].sip_signaling_port = 5060;		// or application defined port for SIP

	ip_virtboard[0].sip_msginfo_mask = IP_SIP_MSGINFO_ENABLE;	// Enabling Access to SIP Header Information
	ip_virtboard[0].reserved = NULL;

	ip_virtboard[0].total_max_calls = _config.getLinesCount();
	ip_virtboard[0].h323_max_calls = 0;
	ip_virtboard[0].sip_max_calls = _config.getLinesCount();
	ip_virtboard[0].E_SIP_dynamic_outbound_proxy_enabled = ENUM_Enabled;

	stack.generalLingerTimer = 0;
	stack.inviteLingerTimer = 0;
	ip_virtboard[0].sip_stack_cfg = &stack;

	return &ipcclibstart;
}

void StartGlobalCall(const ProgramConfig& _config) {
	GC_START_STRUCT gclib_start;

	CCLIB_START_STRUCT cclib_start[3] =
	{
		{ "GC_DM3CC_LIB", NULL },
		{ "GC_H3R_LIB", InitIPVirtboards(_config) },
		{ "GC_IPM_LIB",NULL }
	};

	//	cclib_start[0].cclib_name = "GC_ISDN_LIB";

	gclib_start.num_cclibs = 3;
	gclib_start.cclib_list = cclib_start;
	int gcError = gc_Start(&gclib_start);
}

void StopGlobalCall() {
	gc_Stop();
}

void SetCapability(CGCParamBlock& target_datap, IP_CAPABILITY& t_Capability, int direction) {
	t_Capability.type = GCCAPTYPE_AUDIO;
	t_Capability.direction = direction;
	t_Capability.extra.audio.VAD = GCPV_DISABLE;

	t_Capability.capability = GCCAP_AUDIO_g711Alaw64k;
	t_Capability.extra.audio.frames_per_pkt = 30;
	gc_util_insert_parm_ref(target_datap.get_pointer(), GCSET_CHAN_CAPABILITY, IPPARM_LOCAL_CAPABILITY, sizeof(IP_CAPABILITY), &t_Capability);
}

void SetupOutboundProxy(const TestRunner& _holder, const std::string& _bnumber)
{
	auto pos = _bnumber.find(L'@');

	if (pos == string::npos)
		return;

	string proxyIP = _bnumber.substr(pos + 1);

	CGCParamBlock l_pParmBlk;
	long l_requestID = 0;

	gc_util_insert_parm_val(l_pParmBlk.get_pointer(), IPSET_PROXY_INFO, IPPARM_PROXY_ACTION, sizeof(char), IP_PROXY_SELECT);

	IP_PROXY_INFO proxyinfo;
	strcpy_s(proxyinfo.arrAddr, proxyIP.c_str());
	proxyinfo.eAddrType = ePROXY_ADDRESS_TYPE_IP;

	proxyinfo.eProtocol = ePROXY_PROTOCOL_UDP;
	proxyinfo.usPort = 5060;

	strcpy_s(proxyinfo.arrHostName, proxyIP.c_str());

	// Insert the proxy info structure into the GC Parameter Block
	gc_util_insert_parm_ref_ex(l_pParmBlk.get_pointer(), IPSET_PROXY_INFO, IPPARM_PROXY_INFO, sizeof(IP_PROXY_INFO), (void*)&(proxyinfo));
	// Use the parameter block from above and send it down to the IPCCLIB
	gc_SetConfigData(GCTGT_CCLIB_NETIF, _holder.getBoard(), l_pParmBlk.get_block(), 0, GCUPDATE_IMMEDIATE, &l_requestID, EV_ASYNC);

}

CRN MakeSipIpCall(TestRunner& _holder, LINEDEV _line) {
	auto& config = _holder.getConfig();
	auto& log = _holder.getLog();

	char *IpDisplay = "This is a Display"; /* display data */
	char destAddress[32]; /* destination IP address for MAKECALL_BLK */
	sprintf_s(destAddress, "%s", config.getBNumber().c_str());

	char srcAddress[32] = { 0 };
	sprintf_s(srcAddress, "%03i@%s", _holder.iterate(), config.getANumberIP().c_str());

	char * IpPhoneList = "";

	int rc = 0;
	CRN crn = 0;

	GC_MAKECALL_BLK gcmkbl;
	int MakeCallTimeout = 120;
	/* initialize GCLIB_MAKECALL_BLK structure */
	GCLIB_MAKECALL_BLK gclib_mkbl = { 0 };
	/* set to NULL to retrieve new parameter block from utility function */
	CGCParamBlock target_datap;// = NULL;
	gcmkbl.cclib = NULL; /* CCLIB pointer unused */
	gcmkbl.gclib = &gclib_mkbl;
	/* set GCLIB_ADDRESS_BLK with destination string & type*/
	strcpy_s(gcmkbl.gclib->destination.address, destAddress);
	gcmkbl.gclib->destination.address_type = GCADDRTYPE_TRANSPARENT;

	/* set GCLIB_ADDRESS_BLK with origination string & type*/
	strcpy_s(gcmkbl.gclib->origination.address, srcAddress);
	gcmkbl.gclib->origination.address_type = GCADDRTYPE_TRANSPARENT;

	/* initialize IP_CAPABILITY structure */
	IP_CAPABILITY t_Capability = { 0 };

	/* configure a GC_PARM_BLK with four coders, display, phone list and UUI message: */
	/* specify and insert first capability parameter data for G.7231 coder */
	SetCapability(target_datap, t_Capability, IP_CAP_DIR_LCLTRANSMIT);
	SetCapability(target_datap, t_Capability, IP_CAP_DIR_LCLRECEIVE);

	/*set SIP protocol*/
	gc_util_insert_parm_val(target_datap.get_pointer(), IPSET_PROTOCOL, IPPARM_PROTOCOL_BITMASK, sizeof(char), IP_PROTOCOL_SIP);

	gc_util_insert_parm_ref(target_datap.get_pointer(), IPSET_CALLINFO, IPPARM_DISPLAY, static_cast<unsigned char>(strlen(IpDisplay) + 1), IpDisplay);
	gc_util_insert_parm_ref(target_datap.get_pointer(), IPSET_CALLINFO, IPPARM_PHONELIST, static_cast<unsigned char>(strlen(IpPhoneList) + 1), IpPhoneList);

	char type = IP_CONNECTIONMETHOD_FASTSTART;
	gc_util_insert_parm_ref(target_datap.get_pointer(), IPSET_CALLINFO, IPPARM_CONNECTIONMETHOD, sizeof(char), &type);

	char sip_contact[64] = { 0 };
	sprintf_s(sip_contact, "Contact: <sip:%s>", srcAddress);
	gc_util_insert_parm_ref_ex(target_datap.get_pointer(), IPSET_SIP_MSGINFO, IPPARM_SIP_HDR, (unsigned long)(strlen(sip_contact) + 1), static_cast<void*>(sip_contact));

	char * uui_header = "User-To-User: 0001020304050607080910111213141516171819202122232425262728293031";
	gc_util_insert_parm_ref_ex(target_datap.get_pointer(), IPSET_SIP_MSGINFO, IPPARM_SIP_HDR, (unsigned long)(strlen(uui_header) + 1), static_cast<void*>(uui_header));

	SetupOutboundProxy(_holder, destAddress);

	if (rc == 0) {
		gclib_mkbl.ext_datap = target_datap.get_block();
		rc = gc_MakeCall(_line, &crn, NULL, &gcmkbl, MakeCallTimeout, EV_ASYNC);

		if (rc != 0) {
			wostringstream out;
			out << L"Failed to make call [" << stow(srcAddress) << L"]->[" << stow(destAddress) << L"]";
			log.Log(out.str());
		}
		else {
			_holder.setCallNumber(_line, srcAddress);
			log.LogCallProcessing(L"MAKE CALL", stow(srcAddress) , _line, crn);
		}
	}

	return crn;
}

long OpenBoard() {
	char * strGCDeviceName = ":N_iptB1:P_IP";
	long deviceHandle = 0;
	gc_Open(&deviceHandle, strGCDeviceName, EV_SYNC);

	CGCParamBlock parm_blkp;
	long request_idp;

	gc_util_insert_parm_val(parm_blkp.get_pointer(), IPSET_CONFIG, IPPARM_OPERATING_MODE, sizeof(long), IP_T38_MANUAL_MODIFY_MODE);

	long long_value = 0;
	gc_util_insert_parm_val(parm_blkp.get_pointer(), IPSET_EXTENSIONEVT_MSK, GCACT_SETMSK, GC_VALUE_LONG, long_value);

	gc_util_insert_parm_val(parm_blkp.get_pointer(), GCSET_CALLEVENT_MSK, GCACT_SETMSK, sizeof(long), GCMSK_PROGRESS);
	gc_util_insert_parm_val(parm_blkp.get_pointer(), IPSET_EXTENSIONEVT_MSK, GCACT_ADDMSK, GC_VALUE_LONG, EXTENSIONEVT_SIP_18X_RESPONSE);

	char * pUserToUser = "User-To-User";
	gc_util_insert_parm_ref(parm_blkp.get_pointer(), IPSET_CONFIG, IPPARM_REGISTER_SIP_HEADER, static_cast<unsigned char>(strlen(pUserToUser) + 1), pUserToUser);

	char * pRequestURI = "Request-URI";
	gc_util_insert_parm_ref(parm_blkp.get_pointer(), IPSET_CONFIG, IPPARM_REGISTER_SIP_HEADER, static_cast<unsigned char>(strlen(pRequestURI) + 1), pRequestURI);

	gc_SetConfigData(GCTGT_CCLIB_NETIF, deviceHandle, parm_blkp.get_block(), 0, GCUPDATE_IMMEDIATE, &request_idp, EV_ASYNC);

	return deviceHandle;
}

LINEDEV OpenLine(int nTimeslot) {
	ostringstream stream;
	stream << ":N_iptB1T" << nTimeslot << ":M_ipmB1C" << nTimeslot << ":P_SIP";
	string lineName = stream.str();

	LINEDEV line;
	gc_OpenEx(&line, const_cast<char*>(lineName.c_str()), EV_SYNC, 0);

	CGCParamBlock target_datap;// = NULL;
	IP_CAPABILITY a_DefaultCapability = { 0 };

	SetCapability(target_datap, a_DefaultCapability, IP_CAP_DIR_LCLTRANSMIT);
	SetCapability(target_datap, a_DefaultCapability, IP_CAP_DIR_LCLRECEIVE);

	gc_util_insert_parm_val(target_datap.get_pointer(), IPSET_DTMF, IPPARM_SUPPORT_DTMF_BITMASK, sizeof(char), IP_DTMF_TYPE_INBAND_RTP | IP_DTMF_TYPE_RFC_2833);

	gc_SetUserInfo(GCTGT_GCLIB_CHAN, line, target_datap.get_block(), GC_ALLCALLS);

	try {
		CGCParamBlock t_pParmBlk;
		long request_id;
		gc_util_insert_parm_val(t_pParmBlk.get_pointer(), GCSET_CALLEVENT_MSK, GCACT_ADDMSK, sizeof(long), GCMSK_INVOKEXFER_ACCEPTED);
		gc_SetConfigData(GCTGT_GCLIB_CHAN, line, t_pParmBlk.get_block(), 0, GCUPDATE_IMMEDIATE, &request_id, EV_SYNC);
	}
	catch (...) {
	}

	return line;
}

long EventHandlerStub(unsigned long ulParam) {
	
	auto& testHolder	= TestRunner::getHolder();//*reinterpret_cast<TestParametersHolder *>(ulParam);
	auto& config		= testHolder.getConfig();
	auto& log			= testHolder.getLog();


	METAEVENT meta;
	gc_GetMetaEvent(&meta);

	auto number = stow(testHolder.getNumber(meta.linedev));

	switch (meta.evttype) {
	case GCEV_PROCEEDING:
		log.LogCallProcessing(L"PROCEEDING", number, meta.linedev, meta.crn);
		break;
	case GCEV_TASKFAIL:
		log.LogCallProcessing(L"TASK FAIL", number, meta.linedev, meta.crn);
		break;

	case GCEV_DISCONNECTED:
		log.LogCallProcessing(L"DISCONNECTED", number, meta.linedev, meta.crn);
		gc_DropCall(meta.crn, 16, EV_ASYNC);
		break;

	case GCEV_ALERTING:
		log.LogCallProcessing(L"ALERTING", number, meta.linedev, meta.crn);
		break;

	case GCEV_CONNECTED:
		log.LogCallProcessing(L"CONNECTED", number, meta.linedev, meta.crn);
		break;

	case GCEV_DROPCALL:
		log.LogCallProcessing(L"RELEASING CALL", number, meta.linedev, meta.crn);
		gc_ReleaseCallEx(meta.crn, EV_ASYNC);
		break;

	case GCEV_RELEASECALL:
		log.LogCallProcessing(L"CALL DONE", number, meta.linedev, meta.crn);
		testHolder.decreaseCurrentCalls();
		testHolder.markLineAsFree(meta.linedev);
		break;

	default:
		{
			wostringstream out;
			out << L"EVENT: " << meta.evttype << L" LINEDEV:" << meta.linedev << endl;
			log.Log(out.str());
		}
	break;
	}

	return 0;
}

bool TestRunner::nextMakeCall() {
	
	LINEDEV line = freeLines.pop_front();

	// Let the drivers release call
	this_thread::sleep_for(milliseconds(this->config.getTimeout()));

	try {
		if (iterations > 0) {
			++currentCalls;
			auto crn = MakeSipIpCall(*this, line);
		}
		else if (currentCalls == 0) {
			return false;
		}
	}
	catch (out_of_range&) {
		wcout << L"FAILED to find LINEDEV " << line << endl;
	}

	return true;
}