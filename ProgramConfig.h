#pragma once
#include <string>
#include <atomic>

using namespace std;

class ProgramConfig
{
public:
	ProgramConfig(int argc, char * argv[]) {
		aNumberIP = argv[4];
		destinationNumber = argv[5];
		linesCount = atoi(argv[1]);
		iterations = atoi(argv[2]);
		timeoutOnMakeCall = atoi(argv[3]);
	}

	const string& getANumberIP() const noexcept { return aNumberIP; }
	const string& getBNumber() const noexcept { return destinationNumber; }

	int getTimeout() const noexcept { return timeoutOnMakeCall; }
	int getLinesCount() const noexcept { return linesCount; }
	int getIterations() const noexcept { return iterations; }
	

private:
	string aNumberIP;
	string destinationNumber;
	int timeoutOnMakeCall = 50;
	int linesCount = 2;
	int iterations = 100;
};

