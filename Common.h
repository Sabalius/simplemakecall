#pragma once

#include <gclib.h>
#include <gcip.h>

class CGCParamBlock
{
	GC_PARM_BLKP	m_gcParamBlk;
	bool			m_blAutoDelete;
public:
	CGCParamBlock() : m_gcParamBlk(nullptr), m_blAutoDelete(true)
	{

	}

	CGCParamBlock(GC_PARM_BLKP pParamBlock) : m_gcParamBlk(pParamBlock), m_blAutoDelete(false)
	{
	}

	~CGCParamBlock()
	{
		if (m_blAutoDelete && !is_null())
		{
			gc_util_delete_parm_blk(m_gcParamBlk);
		}
	}

	GC_PARM_BLKP * get_pointer() { return &m_gcParamBlk; }
	GC_PARM_BLKP  get_block() { return m_gcParamBlk; }
	bool is_null() const { return m_gcParamBlk == nullptr; }
};

IPCCLIB_START_DATA * InitIPVirtboards();

void StartGlobalCall();
void StopGlobalCall();

void SetCapability(CGCParamBlock& target_datap, IP_CAPABILITY& t_Capability, int direction);

bool EnableHandler();

bool DisableHandler();
